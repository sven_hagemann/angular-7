import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// deutsches Währungsformat importieren
import { registerLocaleData } from '@angular/common';
import localeDE from '@angular/common/locales/de';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// deutsches Währungsformat laden
registerLocaleData(localeDE);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

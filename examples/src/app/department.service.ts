import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDepartment } from './department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  private departmentsUrl = './assets/data/departments.json';

  constructor(private http: HttpClient) { }

  getDepartments() {
    // console.log(this.http.get<IDepartment[]>(this.departmentsUrl));

    return this.http.get<IDepartment[]>(this.departmentsUrl);
  }
}

import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  errorMsg: any;

  constructor(private employeeService: EmployeeService) { }

  public employees = [];

  ngOnInit() {
    this.employeeService.getEmployees()
    .subscribe( data => this.employees = data,
                (error: HttpErrorResponse) => this.errorMsg = error.message
              );
  }
}

import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../department.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnInit {
  errorMsg: string;
  selectedID: number;

  constructor(private departmentService: DepartmentService,
              private router: Router,
              private route: ActivatedRoute) { }

  public departments = [];

  ngOnInit() {
    this.departmentService.getDepartments()
    .subscribe( data => this.departments = data,
                (error: HttpErrorResponse) => this.errorMsg = error.message
              );
    // first call have not id in its address
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.selectedID = parseInt(params.get('id'), 10);
    });
  }

  navigateToDepartmentDetails(department) {
    // this.router.navigate(['departments', department.id]);
    this.router.navigate([department.id], {relativeTo: this.route});
  }

  isSelected(department) {
    return department.id === this.selectedID;
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { getContext } from '@angular/core/src/render3/discovery_utils';

@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.scss']
})
export class DepartmentDetailComponent implements OnInit {
  departmentId: number;

  constructor(private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    // const id = parseInt(this.route.snapshot.paramMap.get('id'));
    // this.departmentId = id;

    this.route.paramMap.subscribe((params: ParamMap) => {
      this.departmentId = parseInt(params.get('id'), 10);
    });
  }

goPrevious() {
    // this.router.navigate(['/departments', this.departmentId - 1]);
    this.router.navigate(['../', this.departmentId - 1], {relativeTo: this.route});
  }

goNext() {
    this.router.navigate(['../', this.departmentId + 1], {relativeTo: this.route});
  }

gotoDepartments() {
    const selectedID = this.departmentId ? this.departmentId : null;
    // this.router.navigate(['/departments', {id: selectedID}]);
    this.router.navigate(['../', {id: selectedID}], {relativeTo: this.route});
  }

  showOverview() {
    this.router.navigate(['overview'], {relativeTo: this.route});
  }

  showContact() {
    this.router.navigate(['contact'], {relativeTo: this.route});
  }
}
